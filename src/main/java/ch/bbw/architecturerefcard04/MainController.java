package ch.bbw.architecturerefcard04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

@Controller
public class MainController {

	// https://spring.io/guides/gs/uploading-files/

	private StorageService storageService;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	public MainController(StorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("")
	public String homepage(Model model) {
		model.addAttribute("files", storageService.getFiles());
		return "index";
	}


	@PostMapping("/upload")
	public String uploadFile(@RequestParam("file") MultipartFile file,
							 RedirectAttributes attributes,
							 Model model) throws IOException {

		model.addAttribute("files", storageService.getFiles());
		if (!file.isEmpty()) {
			storageService.storeFile(file);
			fileStorageService.storeFile(file);
		}
		return "redirect:";
	}



	/*
	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}
	*/

}
